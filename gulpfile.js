'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

// Compress e optimize images
gulp.task('images', function() {
  return gulp.src('assets/img/*')
    // .pipe($.imagemin())
    .pipe(gulp.dest('dist/assets/img/'));
});

// Compile SASS and minify CSS
gulp.task('styles', function() {
  return gulp.src('assets/scss/main.scss')
    .pipe($.sourcemaps.init())
      .pipe($.sass())
    .pipe($.sourcemaps.write())
    .pipe($.cssmin())
    .pipe($.rename('nv.min.css'))
    .pipe(gulp.dest('dist/assets/css/'));
});

gulp.task('fonts', function() {
  return gulp.src('assets/scss/fonts/*')
    .pipe(gulp.dest('dist/assets/css/fonts/'));
});

// Concat and minify JS
gulp.task('scripts', function() {
  var components = [
    'bower_components/skrollr/src/skrollr.js',
    'assets/js/*.js'
  ];
  return gulp.src(components)
    .pipe($.sourcemaps.init())
      .pipe($.concat('nv.min.js'))
    .pipe($.sourcemaps.write())
    .pipe($.uglify())
    .pipe(gulp.dest('dist/assets/js/'))
});

// Minify HTML
gulp.task('html', function() {
  return gulp.src('index.html')
    .pipe($.htmlmin({collapseWhitespace: true}))
    .pipe($.rename({sufix: 'min'}))
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['images', 'styles', 'fonts', 'scripts', 'html']);
