(function() {
  'use strict';

  // Carousel

  var slides = document.querySelectorAll('.slideshow .slide');
  var slidesLength = slides.length;
  var autoPlaySlide = null;
  var counter = 0;
  var time = 3000;
  var slideMenuItems = document.querySelectorAll('.slideshow .menu .item');

  var removeShowAndActiveClasses = function() {
    for (var i = 0; i < slidesLength; i++) {
      if (slides[i].classList.contains('show')) {
        slides[i].classList.remove('show');
        slideMenuItems[i].classList.remove('active');
        return;
      }
    }
    return;
  };

  var showSlide = function(slideToShow) {
    removeShowAndActiveClasses();
    slides[slideToShow].classList.add('show');
    slideMenuItems[slideToShow].classList.add('active');
  };

  var startAutoPlay = function() {
    if (autoPlaySlide) {
      window.clearInterval(autoPlaySlide);
    }

    autoPlaySlide = window.setInterval(function() {
      counter++;
      var slideToShow = Math.abs(counter % slidesLength);
      showSlide(slideToShow);
    }, time);
  };

  var addOnClickInSlideMenuItem = function() {
    [].forEach.call(slideMenuItems, function(item, index) {
      item.addEventListener('click', function() {
        counter = index;
        startAutoPlay();
        showSlide(index);
      });
    });
  };

  if (slidesLength > 1) {
    startAutoPlay();
    addOnClickInSlideMenuItem();
  }

  // Skrollr

  var s = skrollr.init();

})();
